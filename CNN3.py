import time
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.font_manager as fm
from PIL import Image
import os


os.chdir("C:\\Users\\YU-TING\\Desktop\\Gitlab\\Final_Project\\data")
#讓matplotlib補上中文顯示
myfont = fm.FontProperties(fname='C:/Windows/Fonts/msyh.ttc')
fig = plt.figure()

tf.reset_default_graph()


def dataLoad():
    a=np.ones((1,800*800))
    numData=os.listdir()
    for i in numData:
        if(".png" in i):
            Data=Image.open(i)
            Data_Array=np.array(Data).reshape(-1,800*800)
            a=np.vstack((a,Data_Array))
    a=a[1:,:]
    print("load done")
    return a

def labelDector():
    f=open("key_record.txt")
    a=f.read()
    a=a.split()
    number=0
    for i in a:
        number+=1
        if(i != "'a'" and i != "'d'"):
            print("第{0}張圖有問題".format(number))
    f.close()

def labelLoad():
    f=open("key_record.txt")
    a=f.read()
    a=a.split()
    data=np.ones((1,1))
    for i in a:
        if(i == "'a'"):
            data=np.vstack((data,np.zeros((1,1))))
        elif(i == "'d'"):
            data=np.vstack((data,np.ones((1,1))))
        else:
            data=np.vstack((data,np.ones((1,1))))
    data=data[1:,:]
    f.close()
    return data

"""
定義變數
"""
W = []
b = []

x = tf.placeholder(tf.float32, [None, 640000])
y = tf.placeholder(tf.float32, [None, 1])

"""
定義卷積
"""
def Convolution(inputs, num_of_filters, kernel_size):
    h = tf.layers.conv2d(inputs=inputs, filters=num_of_filters, kernel_size=kernel_size, padding="same", activation=tf.nn.relu)
    return h

"""
定義池化
"""
def Max_pool(conv):
    return  tf.layers.max_pooling2d(inputs=conv, pool_size=[2, 2], strides=2)

"""
定義全聯接層模型
"""
def fc_model(X, n_neurons, W, b):
    for i in range(0, len(n_neurons) - 2):
        X = tf.nn.sigmoid(tf.matmul(X,W[i]) + b[i])
    result = tf.nn.sigmoid(tf.matmul(X, W[-1]) + b[-1])
    return result

"""
定義損失函數，此為交叉熵代價函數
"""
def Cost(y_label, prediction):
    cross_entropy = tf.reduce_mean(tf.square(prediction - y_label))
    return cross_entropy

x_image = tf.reshape(x, [-1, 800, 800, 1])


epoches = 100  #設定迭代次數
learning_rate = 0.5 #學習速率

"""
卷積格式:Convolution(輸入矩陣, filter個數, filter大小)
池化格式:Max_pool(輸入矩陣)
"""
h1 = Convolution(x_image, 5, [15, 15])#第一次卷積
p1 = Max_pool(h1)#第一次池化
p2 = Max_pool(p1)#第一次池化
h2 = Convolution(p2, 5, [11, 11])#第二次卷積
p3 = Max_pool(h2)#第二次池化


"""
將池化後的3維矩陣攤平成2維矩陣
"""
pool_times = 3
pool_flat = tf.reshape(p3, [-1, 100 * 100 * 5])#5是卷積核個數

input_size = 100 * 100 * 5

n_neurons = [input_size, 100, 1]

"""
建構權重及偏差
"""
for i in range(0, len(n_neurons)-1):
    W.append(tf.Variable(tf.truncated_normal([n_neurons[i],n_neurons[i+1]])))
    b.append(tf.Variable(tf.zeros([1.0 ,n_neurons[i+1]])))


prediction = fc_model(pool_flat, n_neurons, W, b)
cross_entropy = Cost(y, prediction)
train = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

#--------------------------------------------train-----------------------------------------

sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

costs = np.ones((epoches+1,1))

"""
開始訓練
"""
Training_X = dataLoad() / 255.0
Training_Y = labelLoad()[:200,:]

t0 = time.clock()

for i in range(epoches+1):
    num_of_pic = i % len(Training_X)
    if(num_of_pic>189):
        num_of_pic=189
    batch_xs = Training_X[num_of_pic:(num_of_pic+10),:]
    batch_ys = Training_Y[num_of_pic:(num_of_pic+10),:]
    sess.run(train, feed_dict = {x: batch_xs, y: batch_ys})
    costs[i] = sess.run(cross_entropy, feed_dict = {x: batch_xs, y: batch_ys})
    print(i,"epoch(es)", " Done!")
        
    

saver = tf.train.Saver()
save_path = saver.save(sess, "my_net/model3.ckpt")

print("總共費時:",time.clock()-t0,"秒")

correct_pred = tf.equal(tf.argmax(prediction[:10], 1), tf.argmax(Training_Y[:10], 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
print("Train Set Accuracy:", sess.run(accuracy, feed_dict={x: Training_X[:10], y: Training_Y[:10]}) * 100, "%")
