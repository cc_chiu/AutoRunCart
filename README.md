In this project we will show you how we made auto-driver realize.

We implemented auto-driver on ASPHALT 8, and drived the car by AI.

BTW, We used "Python 3.6" as our major programming language, and "Tensorflow" as core package.


    This project consists of 3 parts:

    First: Preprocessing Game Screen and Detecting Keyboard-Input for Training Set and Testing Set

    Second: Divide Data into 2 Sets, and Training AI Module 

    Third: Testing and Adjustment

1. Preprocessing Game Screen and Detecting Keyboard-Input for Traning Set and Testing Set 

    At first, as you can see, we used "pyautogui" and "PIL" to capture the game screen and process it, 
    and recorded the detected system keyboard-input.
    And then, we processed these images to matrices as input of neural networks,
    and made those keyboard-input as output of neural networks.

2. Divide Data into 2 Sets, and Training AI Module

    In order to ensure that our module training is successful, we delete those failed training sets.
    After filtering data, we started to put the training set(one of the Sets) into the CNN(Convolutional Neural Network) for starting training.
    Here we used 2 kinds of filters and forth poolling in CNN, and 100 hidden units in full-connected layer.

3. Testing and Adjustment

    After traing, we recorded the weights and those parameters to start our testing.
    We start ASPHALT 8 game and let our AI do all of thing automatically.
    When it finish one of games, we stop to adjust again and again until it perfect.

At last, I would like to thank Switch give our a fantastic time.
Keep Going, Captain Toad!!!!
