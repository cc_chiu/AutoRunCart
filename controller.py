import ctypes
import time

SendInput = ctypes.windll.user32.SendInput                               #要調用的C函式，為SendInput()
PUL = ctypes.POINTER(ctypes.c_ulong)                                     #創立C語言的指標

class KeyBdInput(ctypes.Structure):                                      #SendInput()第二參數的結構
     _fields_ = [("wVk", ctypes.c_ushort),
                 ("wScan", ctypes.c_ushort),
                 ("dwFlags", ctypes.c_ulong),
                 ("time", ctypes.c_ulong),
                 ("dwExtraInfo", PUL)]

class HardwareInput(ctypes.Structure):                                   #SendInput()第二參數的結構，此為KeyboardInput
     _fields_ = [("uMsg", ctypes.c_ulong),
                 ("wParamL", ctypes.c_short),
                 ("wParamH", ctypes.c_ushort)]

class MouseInput(ctypes.Structure):                                      #SendInput()第二參數的結構，此為MouseInput
     _fields_ = [("dx", ctypes.c_long),
                 ("dy", ctypes.c_long),
                 ("mouseData", ctypes.c_ulong),
                 ("dwFlags", ctypes.c_ulong),
                 ("time",ctypes.c_ulong),
                 ("dwExtraInfo", PUL)]

class Input_I(ctypes.Union):                                             #KeyboardInput結構內的Union
     _fields_ = [("ki", KeyBdInput),
                 ("mi", MouseInput),
                 ("hi", HardwareInput)]

class Input(ctypes.Structure):                                           
     _fields_ = [("type", ctypes.c_ulong),
                 ("ii", Input_I)]

def PressKey(hexKeyCode):
     extra = ctypes.c_ulong(0)
     ii_ = Input_I()
     ii_.ki = KeyBdInput( 0, hexKeyCode, 0x0008, 0, ctypes.pointer(extra) )
     x = Input( ctypes.c_ulong(1), ii_ )
     ctypes.windll.user32.SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))



def ReleaseKey(hexKeyCode):
     extra = ctypes.c_ulong(0)
     ii_ = Input_I()
     ii_.ki = KeyBdInput( 0, hexKeyCode, 0x0008 | 0x0002, 0, ctypes.pointer(extra) )
     x = Input( ctypes.c_ulong(1), ii_ )
     ctypes.windll.user32.SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))
     
def start():
    PressKey(30)
    time.sleep(1)
    ReleaseKey(30)
    time.sleep(0.5)
        
def Action(key):
    key=key[0,0]
    action=round(key)
    if(action):
        PressKey(32)
        time.sleep(0.3)
        ReleaseKey(32)
        time.sleep(0.3)
    else:
        PressKey(30)
        time.sleep(0.3)
        ReleaseKey(30)
        time.sleep(0.3)
